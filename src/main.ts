import 'phaser'
// import rawdata from './assets/data.csv'
import convoman from 'joegame-twitter-dialogue-scraper/convos/convo-manifest.json'
// import 'audioworklet-polyfill'
import { joegameFacade, parseCSVRowsToWikiData, shaders, Toner } from 'joegame-lib'
import loadAfterLoad from 'joegame-lib/dist/utils/loadAfterLoad'
import MIDIPlayer from 'phaser-timidity-plugin'
import runCinematicNode from 'joegame-lib/dist/actions/runCinematicNode'
// import addAllNPCsFromLayer from './actions/addAllNPCsFromLayer'

// let jgame=new Phaser.Game(createJoegameConfig({mapjson:"./assets/testmap.json",x: 13, y: 41, callbacks:[], startLevel:true}));
// if (DEVELOPMENT){
//     globalThis.jgame = jgame
// }
//  function testt () {{
//         let wikidata = require('./wikidata.json')
//      return wikidata
//     }
// console.log(testt())

// The first step, create a function that simply takes a global data and a tiled json and returns a game/scene that should have all the assets loaded and ready to go.
// Yes, return a _Game_, I like that, keeps its open.
(async (mapjsonfile: string) => {

    const rawdata = await fetch('assets/data.csv').then(res => res.text())
    const facade = new joegameFacade()
    const game: Phaser.Game = await facade.initGame(parseCSVRowsToWikiData(rawdata))
    await facade.loadMapJSON(game, mapjsonfile)
    await facade.loadAssets(game, mapjsonfile)
    facade.createAnims(game)
    facade.createDepthMap(game, mapjsonfile)
    const level = facade.runLevelScene(game, mapjsonfile)
    // level.scene.cameras.main.fadeOut(0)
    await loadAfterLoad(level.scene, 'convo-manifest', 'assets/convos/convo-manifest.json')
    const player = facade.addPlayerToLevel(level, 3 * level.map.tileWidth, 4 * level.map.tileHeight)
    console.log(game.cache.json.get('gdata'))
    facade.addAllObjectsFromLayer(level, 'Objects')
    facade.addAllObjectsFromLayer(level, 'aboveObjects')
    facade.addAllPlatformsFromLayer(level, 'Platforms')
    // facade.addAllNPCsFromLayer(level, 'NPCs')

    // facade.addAllTweetConvosFromLayer(level, 'TweetConvos').then((convos: any) => {
    //     if (convos) {
    //         // Promise.all(convos.map(conv => conv.runConvo())).then(() => console.log('done loading convos'))
    //         convos.forEach(convo => convo.runConvo())
    //     }
    // })
    facade.createLevelPhysics(level)
    // runCinematicNode(level, 'index')
    level.scene.input.keyboard.on('keydown-A', () => { player.align() })
    // level.scene.input.keyboard.on('keydown-J',()=>{player.jumpUp()})
    level.scene.input.keyboard.on('keydown-J', () => { player.speak('howdy, you are joegame player, howdy howdy') })
    // level.scene.input.keyboard.on('keydown-F', () => { insts.play('gong') })
    level.scene.input.keyboard.on('keydown-K', () => { console.log(player.sprite.anims.currentFrame.textureFrame) })
    level.scene.input.keyboard.on('keydown-O', () => { console.log(player.sprite.anims.currentAnim.frames) })
    level.scene.cameras.main.startFollow(player, false, 0.5, 0.5)
    level.scene.cameras.main.setZoom(4)
    // const pipelines = (game.renderer as Phaser.Renderer.WebGL.WebGLRenderer).pipelines
    // pipelines.addPostPipeline('rain', shaders.RainfallPostFX)
    // level.scene.cameras.main.setPostPipeline(['rain'])
    player.sprite.on('animationrepeat', () => {
        level.toner.play({ inst: 'walk' })
        // console.log(player.x, player.y)
        // console.log(player.getWorldTransformMatrix())
        //
    })
    // level.scene.events.on('speak_sound', (arg) => console.log(arg))
    facade.runCinematicNode(level, "index")
})('assets/maps/desert_to_home.json')

async function playMIDIFile(path: string) {
    const mplayer = await MIDIPlayer.createMIDIPlayer("./gravis.cfg")
    mplayer.load(path)
    return mplayer
}
